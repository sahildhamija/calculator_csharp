﻿namespace ThirdProject
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.FirstNumberTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SecondNumberTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.OutputTextBox = new System.Windows.Forms.TextBox();
            this.CalculateButton = new System.Windows.Forms.Button();
            this.ClearButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.AdditionRadioButton = new System.Windows.Forms.RadioButton();
            this.SubtractionRadioButton = new System.Windows.Forms.RadioButton();
            this.MultiplicationRadioButton = new System.Windows.Forms.RadioButton();
            this.DivisionRadioButton = new System.Windows.Forms.RadioButton();
            this.RemainderRadioButton = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(31, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Enter first number: ";
            // 
            // FirstNumberTextBox
            // 
            this.FirstNumberTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FirstNumberTextBox.Location = new System.Drawing.Point(25, 71);
            this.FirstNumberTextBox.Name = "FirstNumberTextBox";
            this.FirstNumberTextBox.Size = new System.Drawing.Size(192, 26);
            this.FirstNumberTextBox.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(31, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(170, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Enter second number: ";
            // 
            // SecondNumberTextBox
            // 
            this.SecondNumberTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SecondNumberTextBox.Location = new System.Drawing.Point(25, 140);
            this.SecondNumberTextBox.Name = "SecondNumberTextBox";
            this.SecondNumberTextBox.Size = new System.Drawing.Size(192, 26);
            this.SecondNumberTextBox.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(89, 194);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "Output";
            // 
            // OutputTextBox
            // 
            this.OutputTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OutputTextBox.Location = new System.Drawing.Point(25, 226);
            this.OutputTextBox.Name = "OutputTextBox";
            this.OutputTextBox.ReadOnly = true;
            this.OutputTextBox.Size = new System.Drawing.Size(192, 26);
            this.OutputTextBox.TabIndex = 9;
            // 
            // CalculateButton
            // 
            this.CalculateButton.Location = new System.Drawing.Point(25, 303);
            this.CalculateButton.Name = "CalculateButton";
            this.CalculateButton.Size = new System.Drawing.Size(97, 50);
            this.CalculateButton.TabIndex = 7;
            this.CalculateButton.Text = "&Calculate";
            this.CalculateButton.UseVisualStyleBackColor = true;
            this.CalculateButton.Click += new System.EventHandler(this.CalculateButton_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(146, 303);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(95, 50);
            this.ClearButton.TabIndex = 8;
            this.ClearButton.Text = "&Clear";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RemainderRadioButton);
            this.groupBox1.Controls.Add(this.DivisionRadioButton);
            this.groupBox1.Controls.Add(this.MultiplicationRadioButton);
            this.groupBox1.Controls.Add(this.SubtractionRadioButton);
            this.groupBox1.Controls.Add(this.AdditionRadioButton);
            this.groupBox1.Location = new System.Drawing.Point(430, 71);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(244, 248);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Calculation type";
            // 
            // AdditionRadioButton
            // 
            this.AdditionRadioButton.AutoSize = true;
            this.AdditionRadioButton.Location = new System.Drawing.Point(29, 44);
            this.AdditionRadioButton.Name = "AdditionRadioButton";
            this.AdditionRadioButton.Size = new System.Drawing.Size(63, 17);
            this.AdditionRadioButton.TabIndex = 2;
            this.AdditionRadioButton.TabStop = true;
            this.AdditionRadioButton.Text = "Addition";
            this.AdditionRadioButton.UseVisualStyleBackColor = true;
            // 
            // SubtractionRadioButton
            // 
            this.SubtractionRadioButton.AutoSize = true;
            this.SubtractionRadioButton.Location = new System.Drawing.Point(29, 75);
            this.SubtractionRadioButton.Name = "SubtractionRadioButton";
            this.SubtractionRadioButton.Size = new System.Drawing.Size(79, 17);
            this.SubtractionRadioButton.TabIndex = 3;
            this.SubtractionRadioButton.TabStop = true;
            this.SubtractionRadioButton.Text = "Subtraction";
            this.SubtractionRadioButton.UseVisualStyleBackColor = true;
            // 
            // MultiplicationRadioButton
            // 
            this.MultiplicationRadioButton.AutoSize = true;
            this.MultiplicationRadioButton.Location = new System.Drawing.Point(29, 107);
            this.MultiplicationRadioButton.Name = "MultiplicationRadioButton";
            this.MultiplicationRadioButton.Size = new System.Drawing.Size(86, 17);
            this.MultiplicationRadioButton.TabIndex = 4;
            this.MultiplicationRadioButton.TabStop = true;
            this.MultiplicationRadioButton.Text = "Multiplication";
            this.MultiplicationRadioButton.UseVisualStyleBackColor = true;
            // 
            // DivisionRadioButton
            // 
            this.DivisionRadioButton.AutoSize = true;
            this.DivisionRadioButton.Location = new System.Drawing.Point(29, 139);
            this.DivisionRadioButton.Name = "DivisionRadioButton";
            this.DivisionRadioButton.Size = new System.Drawing.Size(62, 17);
            this.DivisionRadioButton.TabIndex = 5;
            this.DivisionRadioButton.TabStop = true;
            this.DivisionRadioButton.Text = "Division";
            this.DivisionRadioButton.UseVisualStyleBackColor = true;
            // 
            // RemainderRadioButton
            // 
            this.RemainderRadioButton.AutoSize = true;
            this.RemainderRadioButton.Location = new System.Drawing.Point(29, 173);
            this.RemainderRadioButton.Name = "RemainderRadioButton";
            this.RemainderRadioButton.Size = new System.Drawing.Size(76, 17);
            this.RemainderRadioButton.TabIndex = 6;
            this.RemainderRadioButton.TabStop = true;
            this.RemainderRadioButton.Text = "Remainder";
            this.RemainderRadioButton.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(800, 485);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.CalculateButton);
            this.Controls.Add(this.OutputTextBox);
            this.Controls.Add(this.SecondNumberTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.FirstNumberTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calculator";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FirstNumberTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox SecondNumberTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox OutputTextBox;
        private System.Windows.Forms.Button CalculateButton;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton RemainderRadioButton;
        private System.Windows.Forms.RadioButton DivisionRadioButton;
        private System.Windows.Forms.RadioButton MultiplicationRadioButton;
        private System.Windows.Forms.RadioButton SubtractionRadioButton;
        private System.Windows.Forms.RadioButton AdditionRadioButton;
    }
}

