﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThirdProject
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            FirstNumberTextBox.Clear();
            SecondNumberTextBox.Clear();
            OutputTextBox.Clear();

            FirstNumberTextBox.Focus();
        }

        private void CalculateButton_Click(object sender, EventArgs e)
        {
            decimal firstNum = Convert.ToDecimal(FirstNumberTextBox.Text);
            decimal secondNum = Convert.ToDecimal(SecondNumberTextBox.Text);
            decimal output = 0;
            if(AdditionRadioButton.Checked == true)
            {
                output = firstNum + secondNum;
            }
            if (SubtractionRadioButton.Checked == true)
            {
                output = firstNum - secondNum;
            }
            if (MultiplicationRadioButton.Checked == true)
            {
                output = firstNum * secondNum;
            }
            if (DivisionRadioButton.Checked == true)
            {
                output = firstNum / secondNum;
            }
            if (RemainderRadioButton.Checked == true)
            {
                output = firstNum % secondNum;
            }

            OutputTextBox.Text = output.ToString();

        }
    }
}
